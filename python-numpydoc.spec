Name:           python-numpydoc
Version:        0.7.0
Release:        2.2%{?dist}
Summary:        Sphinx extension to support docstrings in NumPy format

License:        BSD
URL:            https://pypi.python.org/pypi/numpydoc
Source0:        https://files.pythonhosted.org/packages/source/n/numpydoc/numpydoc-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-nose
BuildRequires:  python%{python3_pkgversion}-sphinx
BuildRequires:  python%{python3_pkgversion}-matplotlib

%description
Numpydoc inserts a hook into Sphinx's autodoc that converts docstrings
following the NumPy/SciPy format to a form palatable to Sphinx.


%package -n     python%{python3_pkgversion}-numpydoc
Summary:        %{summary}
%{?python_provide:%python_provide python%{python3_pkgversion}-numpydoc}
Requires:       python%{python3_pkgversion}-sphinx >= 1.2.3
Requires:       python%{python3_pkgversion}-jinja2 >= 2.3
%description -n python%{python3_pkgversion}-numpydoc
Numpydoc inserts a hook into Sphinx's autodoc that converts docstrings
following the NumPy/SciPy format to a form palatable to Sphinx.

%package -n     python%{python3_other_pkgversion}-numpydoc
Summary:        %{summary}
%{?python_provide:%python_provide python%{python3_other_pkgversion}-numpydoc}
Requires:       python%{python3_other_pkgversion}-sphinx >= 1.2.3
Requires:       python%{python3_other_pkgversion}-jinja2 >= 2.3
%description -n python%{python3_other_pkgversion}-numpydoc
Numpydoc inserts a hook into Sphinx's autodoc that converts docstrings
following the NumPy/SciPy format to a form palatable to Sphinx.


%prep
%autosetup -n numpydoc-%{version}

%build
%py3_build

%install
%py3_install


%check
nosetests-%{python3_version} -v


%files -n python%{python3_pkgversion}-numpydoc
%license LICENSE.txt
%doc README.rst
%{python3_sitelib}/numpydoc
%{python3_sitelib}/numpydoc-%{version}-py?.?.egg-info


%changelog
* Fri Oct 4 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 0.7.0-2.2
- Drop python3.4, python2 support

* Thu Nov 29 2018 Michael Thomas <michael.thomas@LIGO.ORG> - 0.7.0-2.1
- Add python3.6 support

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.7.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Tue Aug 15 2017 Miro Hrončok <mhroncok@redhat.com> - 0.7.0-1
- Updated to 0.7.0 (#1481761)
- Rewrote the spec completely

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.5-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.5-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Mon Dec 19 2016 Miro Hrončok <mhroncok@redhat.com> - 0.5-6
- Rebuild for Python 3.6

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5-5
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.5-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Tue Nov 10 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5-3
- Rebuilt for https://fedoraproject.org/wiki/Changes/python3.5

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Wed Aug 27 2014 Thomas Spura <tomspur@fedoraproject.org> - 0.5-1
- update to 0.5 (#1134171)
- enable python3 subpackage

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Mon Aug  5 2013 Thomas Spura <tomspur@fedoraproject.org> - 0.4-2
- BR python2-devel, python-sphinx, python-nose
- use macro in URL
- disable python3 package for now

* Fri Aug  2 2013 Thomas Spura <tomspur@fedoraproject.org> - 0.4-1
- initial package
